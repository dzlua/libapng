-- the debug mode
if modes("debug") then
    
    -- enable the debug symbols
    set_symbols("debug")

    -- disable optimization
    set_optimize("none")
end

-- the release mode
if modes("release") then

    -- set the symbols visibility: hidden
    set_symbols("hidden")

    -- enable fastest optimization
    set_optimize("fastest")

    -- strip all symbols
    set_strip("all")
end

-- add target
add_target("apng")

    -- set kind
    set_kind("static")

    add_files("src/*.c")
    add_files("src/zlib/*.c")

    add_includedirs("src/zlib")

-- add target
add_target("test")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load6apng.cpp")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")

-- add target
add_target("load1png")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load1png.c")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")

-- add target
add_target("load2png")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load2png.c")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")

-- add target
add_target("load3apng")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load3apng.c")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")

-- add target
add_target("load4apng")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load4apng.c")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")

-- add target
add_target("load5apng")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load5apng.cpp")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")

-- add target
add_target("load6apng")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load6apng.cpp")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")

-- add target
add_target("load7png")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load7png.c")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")
    
-- add target
add_target("load8apng")

    -- set kind
    set_kind("binary")

    -- add deps
    add_deps("apng")

    -- add files
    add_files("test/load8apng.c")

    --add_files("src/*.c")

    -- add links
    add_links("apng")

    -- add link directory
    add_linkdirs("build")

    add_includedirs("src")
    add_includedirs("src/zlib")
    